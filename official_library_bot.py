import telebot
import time
import io
import pickle
import os

TOKEN = '1272524847:AAEZA4A5uLY-tWkOeLRiGgmTTcO84upH25Q'
bot = telebot.TeleBot(TOKEN)

list_category = ('Мультики 30-х', \
                'Математика', 'Физика', 'Экономика', \
                'Компьютерные науки', 'Криптография', 'Сети', 'Электротехника', 'Языки программирования', \
                'Хакерство', 'Шахматы', \
                'Другое')

keyboard_categories = telebot.types.ReplyKeyboardMarkup(row_width=1, resize_keyboard=True)
keyboard_categories.add(*[telebot.types.KeyboardButton(name) for name in ['Искусство', 'Наука', 'Программирование', 'Психология', 'Разработка игр', 'Саморазвитие', 'Хакерство', 'Шахматы']])

keyboard_art = telebot.types.ReplyKeyboardMarkup(row_width=1, resize_keyboard=True)
keyboard_art.add(*[telebot.types.KeyboardButton(name) for name in ['Мультики 30-х', 'Другое', '↩ Категория..']])

keyboard_science = telebot.types.ReplyKeyboardMarkup(row_width=1, resize_keyboard=True)
keyboard_science.add(*[telebot.types.KeyboardButton(name) for name in ['Математика', 'Физика', 'Экономика', '↩ Категория..']])

keyboard_programming = telebot.types.ReplyKeyboardMarkup(row_width=1, resize_keyboard=True)
keyboard_programming.add(*[telebot.types.KeyboardButton(name) for name in ['Компьютерные науки', 'Криптография', 'Сети', 'Электротехника', 'Языки программирования', '↩ Категория..']])

list_books = ''
category = ''
subcategory = ''
subcategory_txt = None
number = 0

@bot.message_handler(commands=['start'])
def start_message(message):
    bot.send_message(message.chat.id, '🔎 Какая категория вам нужна?', reply_markup=keyboard_categories)

    if not os.path.exists('data/' + str(message.chat.id)):
        os.mkdir('data/' + str(message.chat.id))

@bot.message_handler(content_types=['text'])
def choose_book(message):
    global list_books
    global category
    global subcategory
    global subcategory_txt
    global number

    try:
        info = open('data/' + str(message.chat.id) + '/category_info.data', 'rb')
        category = pickle.load(info)
    except IOError:
        pass

    try:
        info = open('data/' + str(message.chat.id) + '/subcategory_info.data', 'rb')
        subcategory = pickle.load(info)
    except IOError:
        pass

    if message.text in list_category:
        list_books = ''        
        subcategory = message.text
        pickle.dump(subcategory, open('data/' + str(message.chat.id) + '/subcategory_info.data', 'wb'))

        subcategory_txt = open('./library/' + category + '/' + subcategory + '.txt')

        while True:
            line = subcategory_txt.readline()
            if len(line) == 0:
                break
            list_books += line

        bot.send_message(message.chat.id, list_books)
        bot.send_message(message.chat.id, '🔢 Напечатайте номер книги!')
    elif message.text == 'Искусство' or message.text == 'Наука' or message.text == 'Программирование':
        category = message.text
        pickle.dump(category, open('data/' + str(message.chat.id) + '/category_info.data', 'wb'))

        choose_category(message)
    elif message.text == '↩ Категория..':
        bot.send_message(message.chat.id, '🔎 Какая категория вам нужна?', reply_markup=keyboard_categories)
    elif int(message.text) > 0:
        number = int(message.text)
        send_book(message)
        subcategory_txt.close()
    else:
        pass

def choose_category(message):
    if message.text == 'Искусство':
        bot.send_message(message.chat.id, '🔤 Выберите подкатегорию!', reply_markup=keyboard_art)
    elif message.text == 'Наука':
        bot.send_message(message.chat.id, '🔤 Выберите подкатегорию!', reply_markup=keyboard_science)
    elif message.text == 'Программирование':
        bot.send_message(message.chat.id, '🔤 Выберите подкатегорию!', reply_markup=keyboard_programming)

def send_book(message):
    global list_books
    global category
    global subcategory
    global subcategory_txt
    global number

    if subcategory != '' and number != 0 and number <= int(len(open('./library/' + category + '/' + subcategory + '.txt').readlines())):

        subcategory_txt = open('./library/' + category + '/' + subcategory + '.txt')

        for i in range(number):
            line = subcategory_txt.readline()

            if i + 1 == number:
                break
            
        line = line.replace(str(number) + '. ', '')
        line = line.replace('\n', '')
        print(line)

        bot.send_message(message.chat.id, '📨 Идет отправка...')
        book = open('./library/' + category + '/' + subcategory + '/' + line + '.pdf', 'rb')
        bot.send_document(message.chat.id, book, timeout=10000)

        book.close()
        number = 0
    elif subcategory != '' and number > int(len(open('./library/' + category + '/' + subcategory + '.txt').readlines())):
        bot.send_message(message.chat.id, '⛔ Такого номера нет в списке..')
        
        number = 0
        
bot.polling()